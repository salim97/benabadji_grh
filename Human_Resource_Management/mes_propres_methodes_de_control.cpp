/*

std date is yyyy-mm-dd


*/

#include "mainwindow.h"
//#include "ui_mainwindow.h"

//TODO WARNING check all posibiliti of erreur please and optimaze more the source code and make commnter
QString MainWindow::formatdatestringforcode( QString date )
{
    // ddmmyy
    QString result = "" + date[8] +  date[9] + date[5] + date[6] + date[2] + date[3] ;
    return result ;
}
bool MainWindow::checkifitsdate(QString date)
{

    QString tmp ;
    int value = 0 , counter = 0 ;

    counter = charactercounter(date , '/' );

    if ( counter == 0 )
    counter = charactercounter(date , '-' );


    if ( counter == 2 )
    {

        tmp = date.replace("-","").replace("/","") ;
        bool ok;
        value = tmp.toInt(&ok) ;
        if (ok && value > 100000)
        {
            return true ;
        }
        else
        {
            return false ;
        }
    }
    else
    {
        return false ;
    }

}

QString MainWindow::convert_to_std_date(QString datein)
{
    QString dateout = "" ;
    qDebug() << "input date is : " + datein ;

    if ( checkifitsdate( datein ) )
    {
        if ( datein.contains("/") )
        {
            dateout = std_converter( datein , '/' ) ;
        }
        else
        {
            dateout = std_converter( datein , '-' ) ;
        }

        return dateout ;
    }
    else
    {
        qDebug() << "sorry it's not a date" ;
        return dateout ;
    }

}
QString MainWindow::std_converter( QString datein , char separator )
{
    QString dateout = "" ;
    QStringList tmp01 = datein.split(separator ,QString::KeepEmptyParts);

    QString tmp02 = tmp01[0] ;
    QString tmp03 = tmp01[2] ;
    QString tmp04 = tmp01[1] ;
    if ( tmp02.isEmpty() || tmp03.isEmpty() || tmp04.isEmpty() )
        return dateout ;

    if ( tmp02.length() == 4  )
    {
        dateout =  yyyymmdd(tmp01) ;
    }
    else
    {
        if ( tmp03.length() == 4)
        dateout = ddmmyyyy(tmp01) ;
    }

    return dateout ;
}
QString MainWindow::yyyymmdd(QStringList datein)
{
    return datein[0] + "-" + tadwir(datein[1]) + "-" + tadwir(datein[2]) ;
}

QString MainWindow::ddmmyyyy(QStringList datein)
{
    return datein[2] + "-" + tadwir(datein[1]) + "-" + tadwir(datein[0]) ;
}
QString MainWindow::tadwir(QString num)
{
    if ( num.length() == 1 )  num = "0" + num ;

    return num ;
}
int MainWindow::charactercounter(QString date , char a)
{
    int counter = 0 ;
    for ( int i = 0 ; i < date.length() ; i++ )
    {
        if ( date[i] == a )
        {
            counter++ ;
        }
    }
    return counter ;
}
QString MainWindow::dateconfigdown( QString date , int x ) // dd/mm/yyyy
{
    QStringList datelist ;

    if ( date.contains("-"))
    datelist = date.split("-" ,QString::KeepEmptyParts);
    else
    datelist = date.split("/" ,QString::KeepEmptyParts);

    QString tmp ;
            tmp = datelist[1] ;
    int imonth = tmp.toInt() ;
            tmp = datelist[2] ;
    int iyears = tmp.toInt() ;

    for ( int i = 0 ; i < x ; i++)
    {
        if ( imonth == 1 )
        {
            imonth = 12 ;
            iyears-- ;
        }
        else
        {
            imonth-- ;
        }
    }
    QString result ;

    if ( imonth < 10)
    result = "0" + QString::number(imonth) + "/" + QString::number(iyears) ;
    else
    result = QString::number(imonth) + "/" + QString::number(iyears) ;

    return result ;
}
QString MainWindow::dateconfigup( QString date , int x ) // yyyy-mm-dd
{
    QStringList datelist ;

    if ( date.contains("-"))
        datelist = date.split("-" ,QString::KeepEmptyParts);
    else
        datelist = date.split("/" ,QString::KeepEmptyParts);

    QString tmp ;

    tmp = datelist[0] ;
    int imonth = tmp.toInt() ;

    tmp = datelist[1] ;
    int iyears = tmp.toInt() ;

    for ( int i = 0 ; i < x ; i++)
    {
        if ( imonth == 12 )
        {
            imonth = 1 ;
            iyears++ ;
        }
        else
        {
            imonth++ ;
        }
    }
    QString result ;
    if ( imonth < 10)
        result = "0" + QString::number(imonth) + "/" + QString::number(iyears) ;
    else
        result = QString::number(imonth) + "/" + QString::number(iyears) ;

    return result ;
}


