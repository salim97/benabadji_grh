#ifndef CHIFADA_H
#define CHIFADA_H

#include <QDialog>

namespace Ui {
class chifada;
}

class chifada : public QDialog
{
    Q_OBJECT

public:
    explicit chifada(QWidget *parent = 0);
    ~chifada();
    QString value ;
private slots:
    void on_pushButton_clicked();



private:
    Ui::chifada *ui;
};

#endif // CHIFADA_H
