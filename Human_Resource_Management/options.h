#ifndef OPTIONS_H
#define OPTIONS_H

#include <QDialog>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QDir>
namespace Ui {
class Options;
}

class Options : public QDialog
{
    Q_OBJECT


public:
    explicit Options(QWidget *parent = 0);

    ~Options();
    QString rec="setting.ini";

private slots:


    void on_TB_Cancel_clicked();

    void on_TB_OK_clicked();

    void on_TB_Apply_clicked();

    void on_restart_clicked();

private:
    Ui::Options *ui;
    void savesettingtofile() ;
    void readsettingfromfile(QString) ;

};

#endif // OPTIONS_H
