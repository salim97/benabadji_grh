#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "xlsxdocument.h"
#include "chifada.h"

#include <QMainWindow>
#include <QDebug>
#include <QString>
#include <QStringList>
#include <QStringList>
#include <QFileDialog>
//#include <QMap>
#include <QHash>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QKeyEvent>
#include <QDesktopServices>
#include <QDate>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QHash<int , QString> DBmap[16] ;
    QHash<int , QString> DBmapbackup ;
    QHash<int , int> Tableconvertiseur ;
    int DBmaplength ;
    QStringList filenames ;

    int currentindex;

    QString rec="setting.ini";
    QStringList part[5] ;
    QDate cd ;
    QString today ;

    QString nameofDIR ;
    QString pathfordel ;
    QString path ;          /// goto main
    QString pathofDIR ;
    QString opendir  ;
    QString QDSpath ;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionOuvrir_un_fichier_triggered();

    void on_TB_sreach_clicked();

    void on_actionConfiguration_triggered();

    void on_actionNext_Page_triggered();

    void on_actionBack_Page_triggered();

    void on_sreachtype_currentIndexChanged(int index);

    void on_actionAttestation_triggered();

    void on_actionCertificat_triggered();

    void on_actionCarte_Chifa_CNAS_triggered();

    void on_actionAllocation_Familial_CNAS_triggered();

    void on_actionBulletin_De_Paie_triggered();

private:
    Ui::MainWindow *ui;

    bool checkifitsdate(QString) ;
    bool fexists(QString);

    QString formatdatestringforcode(QString) ;
    QString codeofworker(QString  , QString  , QString  , int *, bool ) ;
    QString tadwir(QString num) ;
    QString amerA(int);
    QString amerB();
    QString fullnumsocial(QString) ;
    QString fullnameoftitle(QString) ;
    QString getaddress(QString);
    QString chatakhdemsocite(QString title) ;
    QString dateconfigup(QString,int) ;
    QString dateconfigdown(QString,int) ;
    QString getkholsa() ;
    QString getmontant(QString) ;

    int howmuchwork(QString date1 ) ;
    int howmuchworktoend(QString date1 ) ;

    void import_to_local_database(QStringList) ;
    void printout(int) ;
    void initconfigfile();
    void readsettingfromfile();
    void enablecompont();
    void disablecompont() ;
    void keyPressEvent ( QKeyEvent * event );
    void deltall();
    void printexcelates(int) ;
    void printexcelCNAS(int) ;
    void printexcelCNASfamilial(int) ;
    void printexcelCER(int);
    void printexcelbulletin_de_paie(int) ;
    void deletalldatabase();
    void reflesh();
    QString convert_to_std_date(QString);
    QString yyyymmdd(QStringList);
    QString ddmmyyyy(QStringList);
    int charactercounter(QString , const char);
    QString std_converter(QString , char);
};

#endif // MAINWINDOW_H
