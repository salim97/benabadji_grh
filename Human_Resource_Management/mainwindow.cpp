/*
"RF" => SARL Ramy Food
"TF" => SARL Taiba Food
"UD" => SARL UNIVERS DETERGENT
"UC" => SARL UNIVER COSMETIQUE
"ES" => EURL SOFTY

DBmap[0] // addres
DBmap[1] // fin travail
DBmap[2] // debut travail
DBmap[3] // numero security social
DBmap[4] // lieu de naisence
DBmap[5] // date naisence
DBmap[6] // travail
DBmap[7] // nom
DBmap[8] // prenom
DBmap[9] // mtzawej wla la
DBmap[10] // quel entrepise ykhdem fiha
DBmap[11] // code de travaieur

*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <options.h>

#include <QTime>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    cd  = QDate::currentDate();
    today = cd.toString("dd/MM/yyyy") ;

    /*-------------TODO ------------*/
    nameofDIR= "mesDocument" ;
    path = QDir::toNativeSeparators(QApplication::applicationDirPath());
    pathofDIR = path + "\\" + nameofDIR ;
    opendir = pathofDIR  ;
    pathfordel = QApplication::applicationDirPath()+ "/" + nameofDIR ;
    QDSpath = "file:///" + opendir.replace("\\","/") ;
    initconfigfile();
    readsettingfromfile();
    disablecompont();
    qDebug() << QDSpath ;
//    QString pathh = "file:///C:/Archive/rabi3/liste des employeurs ramy food.xlsx" ;
//    QDesktopServices::openUrl(QUrl(pathh, QUrl::TolerantMode));

    //qDebug() << "output date is : " + convert_to_std_date("2014/0/0") ;

}

MainWindow::~MainWindow()
{
    delete ui;
    deletalldatabase();
}

void MainWindow::on_actionOuvrir_un_fichier_triggered()
{
    QString filter = "Excel file XLSX (*.xlsx) " ;
    QStringList tmp1 = QFileDialog::getOpenFileNames(this,"open a file","C:/Archive/rabi3/Human Resource Management/",filter );
    if ( tmp1.isEmpty() ) return ;

    filenames = tmp1 ; // i am stupid thing TODO : find solution for this
    QTime myTimer;
    myTimer.start();

    import_to_local_database(filenames) ;
    qDebug() << "time took to import data is : " <<  myTimer.elapsed();
    enablecompont();
}

void MainWindow::on_TB_sreach_clicked()
{
    //qDeleteAll(DBmapbackup);
    DBmapbackup.clear();
    Tableconvertiseur.clear();;

    QString a , b ;

    b = amerB() ;
    int i ,j  ;
    for ( i= 0 , j = 0  ; i < DBmaplength ; i++ )
    {
      a = amerA(i) ;

      if ( a.contains(b) )
      {
        printout(i) ;
        DBmapbackup.insert(j , ui->textBrowser->toPlainText() ) ;
        Tableconvertiseur.insert(j , i );
        j++ ;

      }

    }

    currentindex = 0 ;
    reflesh() ;

}

void MainWindow::on_actionConfiguration_triggered()
{
    Options config ;
    config.setModal(true);
    config.exec();

    readsettingfromfile();
    import_to_local_database(filenames) ;
}

void MainWindow::on_actionNext_Page_triggered()
{
    currentindex++ ;
    reflesh() ;
}

void MainWindow::on_actionBack_Page_triggered()
{
    currentindex-- ;
    reflesh() ;
}

void MainWindow::on_sreachtype_currentIndexChanged(int index)
{
    if ( index == 2 || index == 3 )
    {
        ui->label_example_date_input->setVisible(true);
        ui->lineEditDay->setVisible(true);
        ui->lineEditMonth->setVisible(true);
        ui->lineEditYear->setVisible(true);
        ui->label_1->setVisible(true);
        ui->label_2->setVisible(true);
        ui->lineEditString->setVisible(false);

    }
    else
    {
        ui->lineEditString->setVisible(true);
        ui->label_example_date_input->setVisible(false);
        ui->lineEditDay->setVisible(false);
        ui->lineEditMonth->setVisible(false);
        ui->lineEditYear->setVisible(false);
        ui->label_1->setVisible(false);
        ui->label_2->setVisible(false);
    }
}

void MainWindow::on_actionAttestation_triggered()
{
    printexcelates(currentindex);
}

void MainWindow::on_actionCertificat_triggered()
{
    printexcelCER(currentindex);
}

void MainWindow::on_actionCarte_Chifa_CNAS_triggered()
{
    printexcelCNAS(currentindex);
}

void MainWindow::on_actionAllocation_Familial_CNAS_triggered()
{
    printexcelCNASfamilial(currentindex);
}

void MainWindow::on_actionBulletin_De_Paie_triggered()
{
    printexcelbulletin_de_paie(currentindex);
}
