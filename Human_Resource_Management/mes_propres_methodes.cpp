#include "mainwindow.h"
#include "ui_mainwindow.h"
//TODO :: disable mainwindows when reading flies
/*
"RF" => SARL Ramy Food
"TF" => SARL Taiba Food
"UD" => SARL UNIVERS DETERGENT
"UC" => SARL UNIVER COSMETIQUE
"ES" => EURL SOFTY

DBmap[0] // addres
DBmap[1] // fin travail
DBmap[2] // debut travail
DBmap[3] // numero security social
DBmap[4] // lieu de naisence
DBmap[5] // date naisence
DBmap[6] // travail
DBmap[7] // nom
DBmap[8] // prenom
DBmap[9] // mtzawej wla la
DBmap[10] // title , quel entrepise ykhdem fiha 2 caracter
DBmap[11] // code de travaieur
DBmap[12] // full name of title
DBmap[13] // numero social de l'entreprise
DBmap[14] // address
DBmap[15] // chatakhdem el moassasa
*/

void MainWindow::initconfigfile()
{
    if (!fexists("setting.ini"))
    QFile::copy(":/setting/Setting File/setting.ini", QDir::currentPath()+"/"+rec);
    QFile myfile(QDir::currentPath()+"/"+rec);
    myfile.setPermissions(QFile::ReadOwner |QFile::WriteOwner ) ;
    qDebug() << opendir ;

    qDebug() << QDir(opendir).exists() ;
    if( !QDir(opendir).exists() )
    {
       qDebug() << " does not exist so we creat : \n" << opendir ;
       QDir().mkdir(opendir);
    }
}
void MainWindow::readsettingfromfile()
{

    QFile mfile(rec) ;
    if(!mfile.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "could not open file for read mode" ;
        QMessageBox::warning(this,"erreur","could not open file for read mode");
        return ;
    }
    QTextStream in(&mfile);
    for (int i = 0 ; !in.atEnd() ; i++ )
    {
        QString line = in.readLine();
        part[i] = line.split("=", QString::KeepEmptyParts) ;
    }

}

void MainWindow::printout(int index)
{
    QString text , tmp01 , tmp02;

    tmp01 = QString::number(howmuchworktoend( DBmap[2][index] ) )  ;
    //tmp02 = QString::number(howmuchwork( DBmap[2][index] ) )  ;

    text = "resulta :\n"
           "               socite             : " + DBmap[12][index] + "\n"
           "               address            : " + DBmap[14][index] + "\n"
           "               socite code        : " + DBmap[13][index] + "\n"
           "\n==============================================================\n\n"
           "               CODE TRAVAILEUR    : " + DBmap[11][index] + "\n"
           "               Nom                : " + DBmap[7][index] + "\n"
           "               Prenom             : " + DBmap[8][index] + "\n"
           "               Date de Naissance  : " + DBmap[5][index] + "\n"
           "               Lieu de Naissance  : " + DBmap[4][index] + "\n"
           "               Demeurant a        : " + DBmap[0][index] + "\n"
           "               N Securite Sociale : " + DBmap[3][index] + "\n"
           "               Travial            : " + DBmap[6][index] + "\n"
           "               situation familiyel: " + DBmap[9][index] + "\n"
           "               debut de travail   : " + DBmap[2][index] + "\n"
           "               fin de travail     : " + DBmap[1][index] + "\n"
           "               nombre de mois travail est :   " + tmp01 + "\n" ;
    ui->textBrowser->setText(text.toStdString().data());

}

void MainWindow::printexcelates(int index  )
{
    qDebug() << index << " <=> " << Tableconvertiseur[index] ;
    index = Tableconvertiseur[index] ;

    QXlsx::Document xlsx(":/excel/Example xlsx files/exmpler for attestation.xlsx");


    xlsx.write("C11" , DBmap[15][index] + "")  ;


    xlsx.write("D9" , DBmap[12][index] + "")  ;
    xlsx.write("D19" , DBmap[7][index] + "")  ;
    xlsx.write("D21" , DBmap[8][index] + "") ;
    xlsx.write("D23" , DBmap[5][index] + "") ;
    xlsx.write("D25" , DBmap[4][index] + " - " + DBmap[4][index] ) ;
    xlsx.write("D27" , DBmap[0][index] + "") ;
    xlsx.write("D30" , DBmap[3][index] + "") ;
    xlsx.write("D32" , DBmap[11][index] + "") ;
    xlsx.write("E36" , DBmap[6][index] + "") ;
    xlsx.write("E38" , DBmap[2][index] + "") ;
    xlsx.write("G43" , today + "") ;

    deltall();
    xlsx.saveAs(opendir+"\\attestation de travail " + DBmap[7][index] + "_" + DBmap[8][index] + " .xlsx");

    //QThread::msleep(500);

    //    QString pathh = "file:///C:/Archive/rabi3/liste des employeurs ramy food.xlsx" ;
    QString tmp01 = QDSpath + "//attestation de travail " + DBmap[7][index] + "_" + DBmap[8][index] + " .xlsx" ;
    QDesktopServices::openUrl(QUrl(tmp01, QUrl::TolerantMode));

    QDesktopServices::openUrl(QUrl::fromLocalFile(opendir ) );
}
void MainWindow::printexcelCER( int index  )
{
    index = Tableconvertiseur[index] ;
    QXlsx::Document xlsx(":/excel/Example xlsx files/exmpler for certafica.xlsx");


    xlsx.write("C11" , DBmap[15][index] + "")  ;

    xlsx.write("D9" , DBmap[12][index] + "")  ;
    xlsx.write("D19" , DBmap[7][index] + "")  ;
    xlsx.write("D21" , DBmap[8][index] + "") ;
    xlsx.write("D23" , DBmap[5][index] + "") ;
    xlsx.write("D25" , DBmap[4][index] + " - " + DBmap[4][index] ) ;
    xlsx.write("D27" , DBmap[0][index] + "") ;
    xlsx.write("D30" , DBmap[3][index] + "") ;
    xlsx.write("D32" , DBmap[11][index] + "") ;
    xlsx.write("E36" , DBmap[6][index] + "") ;
    xlsx.write("E38" , DBmap[2][index] + "") ;
    xlsx.write("H38" , DBmap[1][index] + "") ;

    xlsx.write("G43" , today + "") ;


    deltall();
    xlsx.saveAs(opendir+"\\certafica de travail " + DBmap[7][index]+ "_"+ DBmap[8][index] + " .xlsx");

    //QThread::msleep(500);
    QString tmp01 = QDSpath + "//certafica de travail " + DBmap[7][index] + "_" + DBmap[8][index] + " .xlsx" ;
    QDesktopServices::openUrl(QUrl(tmp01, QUrl::TolerantMode));

    QDesktopServices::openUrl(QUrl::fromLocalFile(opendir ) );

}

void MainWindow::printexcelCNAS( int index  )
{

    index = Tableconvertiseur[index] ;
    QXlsx::Document xlsx(":/excel/Example xlsx files/exmpler for chefa.xlsx");

    xlsx.write("Z22" , DBmap[11][index] + " " )  ;
    xlsx.write("N25" , DBmap[12][index] + " " )  ;

    xlsx.write("H40" , DBmap[7][index] + " " )  ; // nom
    xlsx.write("I46" , DBmap[8][index] + " " ) ; // prenom
    xlsx.write("I49" , DBmap[5][index] + " " ) ; // date de naissence
    xlsx.write("V49" , DBmap[4][index] + " " ) ;     // win zad de naissence
    xlsx.write("J52" , DBmap[0][index] + " " ) ; // win yaskon
    xlsx.write("AA43" , DBmap[3][index] + " " ) ; // numero securite social
    xlsx.write("K55" , DBmap[6][index] + " " ) ;
    xlsx.write("AF64" , DBmap[2][index] + " " ) ;


    xlsx.selectSheet("page2");

    QString kholsa = getkholsa() ;
    QString motant = getmontant(kholsa) ;

    xlsx.write("CC38" , today + " ") ;

    int m = howmuchwork(DBmap[2][index]) ;
    if ( m >= 3 ) m = 3 ; // ktebli gi 3 line berk
    qDebug() << m ;
    QString tmp ;
    for ( int i = m , j = 0 ; i >= 1 ; i-- , j+=2 )
    {
        tmp = dateconfigdown(today , i ) ;
        xlsx.write("C"+QString::number(j+12) , tmp + " ") ;
        xlsx.write("U"+QString::number(j+12) , "22") ;
        xlsx.write("AL"+QString::number(j+12) , "/") ;
        xlsx.write("BF"+QString::number(j+12) , kholsa.toInt() ) ;
        xlsx.write("BY"+QString::number(j+12) , "=BF"+QString::number(j+12)+"*0.09" ) ;
    }

  deltall();

    xlsx.saveAs(opendir+"\\carte chifa " + DBmap[7][index]+ "_" + DBmap[8][index] + " .xlsx");

    QString tmp01 = QDSpath + "//carte chifa " + DBmap[7][index] + "_" + DBmap[8][index] + " .xlsx" ;
    QDesktopServices::openUrl(QUrl(tmp01, QUrl::TolerantMode));

    QDesktopServices::openUrl(QUrl::fromLocalFile(opendir ) );

}
void MainWindow::printexcelCNASfamilial( int index  )
{
    index = Tableconvertiseur[index] ;
    QXlsx::Document xlsx(":/excel/Example xlsx files/exmpler for chefa.xlsx");


    xlsx.write("Z22" , DBmap[11][index] + "")  ;
    xlsx.write("N25" , DBmap[12][index] + "")  ;

    xlsx.write("H40" , DBmap[7][index] + "")  ; // nom
    xlsx.write("I46" , DBmap[8][index] + "") ; // prenom
    xlsx.write("I49" , DBmap[5][index] + "") ; // date de naissence
    xlsx.write("V49" , DBmap[4][index] ) ;     // win zad de naissence
    xlsx.write("J52" , DBmap[0][index] + "") ; // win yaskon
    xlsx.write("AA43" , DBmap[3][index] + "") ; // numero securite social
    xlsx.write("K55" , DBmap[6][index] + "") ;
    xlsx.write("AF64" , DBmap[2][index] + "") ;


    QString tmp ;
    xlsx.selectSheet("page2");

    QString kholsa = getkholsa() ;
    QString motant = getmontant(kholsa) ;

    xlsx.write("CC38" , today + " ") ;

    int m = howmuchwork(DBmap[2][index]) ;
    if ( m >= 12 ) m = 12 ;
    qDebug() << m ;
    for ( int i = m , j = 0 ; i >= 1 ; i-- , j += 2 )
    {
        tmp = dateconfigdown(today , i ) ;
        xlsx.write("C"+QString::number(j+12) , tmp + " ") ;
        xlsx.write("U"+QString::number(j+12) , "22") ;
        xlsx.write("AL"+QString::number(j+12) , "/") ;
        xlsx.write("BF"+QString::number(j+12) , kholsa ) ;
        xlsx.write("BY"+QString::number(j+12) , "=BF"+QString::number(j+12)+"*0.09" ) ;
    }


    deltall();
    xlsx.saveAs(opendir+"\\Allocation Familial CNAS " + DBmap[7][index]+ "_" + DBmap[8][index] + " .xlsx");

    QString tmp01 = QDSpath + "//Allocation Familial CNAS " + DBmap[7][index] + "_" + DBmap[8][index] + " .xlsx" ;
    QDesktopServices::openUrl(QUrl(tmp01, QUrl::TolerantMode));

    QDesktopServices::openUrl(QUrl::fromLocalFile(opendir ) );
}

void MainWindow::printexcelbulletin_de_paie( int index  )
{
    index = Tableconvertiseur[index] ;
    QXlsx::Document xlsx(":/excel/Example xlsx files/exmpler bulletin de paie.xlsx");

    QString kholsa = getkholsa() ;

    if ( DBmap[9][index].contains("M") )
        xlsx.write("F16" , DBmap[9][index] + "   N/ENF :") ; // matzawej wla la
    else
        xlsx.write("F16" , DBmap[9][index] + " ") ; // matzawej wla la

    xlsx.write("B13" , DBmap[12][index] + " ")  ; // isem moasasa
    xlsx.write("F14" , DBmap[7][index] + " ")  ; // nom
    xlsx.write("F15" , DBmap[8][index] + " ") ; // prenom
    xlsx.write("A20" , "NO.S. SLE: " + DBmap[3][index] + " ") ; // numero securite social
    xlsx.write("G18" , DBmap[2][index] + " ") ; //  winta bda yakhdem
    xlsx.write("C18" , DBmap[6][index] + " ") ; // khadma kidayra
    xlsx.write("A15" , "N.EMPLOYEUR : " + DBmap[13][index] + " ") ; // numero de sociati
    xlsx.write("G13" , DBmap[11][index] + " ") ; // code ta3 khadem
    xlsx.write("G34" , today + " ") ; // la date ta3 lyoum
    xlsx.write("F22" , kholsa + " ") ; // kholsa

    deltall();
    xlsx.saveAs(opendir+"\\bulletin de paie " + DBmap[7][index]+ "_" + DBmap[8][index] + " .xlsx");


    QString tmp01 = QDSpath + "//bulletin de paie " + DBmap[7][index] + "_" + DBmap[8][index] + " .xlsx" ;
    QDesktopServices::openUrl(QUrl(tmp01, QUrl::TolerantMode));

    QDesktopServices::openUrl(QUrl::fromLocalFile(opendir ) );

}

void MainWindow::import_to_local_database(QStringList filenames)
{
    QMessageBox msgBox(this);
    msgBox.setWindowTitle("Loading ....");
    msgBox.setText("Reading Files, please wait ...");
    msgBox.setWindowModality(Qt::NonModal);
    msgBox.show();

    DBmaplength = 0 ;
    for ( int i = 0 ; i < filenames.length() ; i++)
    {

        QXlsx::Document xlsx(filenames[i]);

        for ( int j = 2 , ihelpme = 1 ;  !xlsx.read("A" + QString::number(j) ).toString().isEmpty() ; j++ , ihelpme++ , DBmaplength++ )
        {
            DBmap[0].insert(DBmaplength , xlsx.read("A" + QString::number(j) ).toString() ) ;// addres
            DBmap[1].insert(DBmaplength , convert_to_std_date(xlsx.read("B" + QString::number(j) ).toString() ) ) ;// fin travail
            DBmap[2].insert(DBmaplength , convert_to_std_date(xlsx.read("C" + QString::number(j) ).toString() ) ) ;// debut travail
            DBmap[3].insert(DBmaplength , xlsx.read("D" + QString::number(j) ).toString() ) ;// numero security social
            DBmap[4].insert(DBmaplength , xlsx.read("E" + QString::number(j) ).toString() ) ;// lieu de naisence
            DBmap[5].insert(DBmaplength , convert_to_std_date(xlsx.read("F" + QString::number(j) ).toString() ) ) ;// date naisence
            DBmap[6].insert(DBmaplength , xlsx.read("G" + QString::number(j) ).toString() ) ;// travail
            DBmap[7].insert(DBmaplength , xlsx.read("H" + QString::number(j) ).toString() ) ;// nom
            DBmap[8].insert(DBmaplength , xlsx.read("I" + QString::number(j) ).toString() ) ;// prenom
            DBmap[9].insert(DBmaplength , xlsx.read("K" + QString::number(j) ).toString() ) ;// mtzawej wla la
            DBmap[10].insert(DBmaplength , xlsx.read("L" + QString::number(j) ).toString() ) ;// title , quel entrepise ykhdem fiha
            if ( j == 2 )
            DBmap[11].insert(DBmaplength , codeofworker(DBmap[10][DBmaplength] , "", DBmap[2][DBmaplength] , &ihelpme , true) ) ; //code de travaieur
            else
            DBmap[11].insert(DBmaplength , codeofworker(DBmap[10][DBmaplength] , DBmap[2][DBmaplength-1 ] , DBmap[2][DBmaplength] , &ihelpme , false) ) ;

            DBmap[12].insert(DBmaplength , fullnameoftitle( DBmap[10][DBmaplength] ) ) ; // full name of title
            DBmap[13].insert(DBmaplength , fullnumsocial( DBmap[10][DBmaplength] ) ) ; // numero social de l'entreprise
            DBmap[14].insert(DBmaplength , getaddress( DBmap[10][DBmaplength] ) ) ; // address
            DBmap[15].insert(DBmaplength , chatakhdemsocite(DBmap[10][DBmaplength]) ) ; // chatakhdem el moassasa
        }

    }

    //for ( int i = 0 ; i < DBmaplength ; i++ ) qDebug() << DBmap[10][i] + " <=> " + DBmap[2][i] + " <=> " + DBmap[11][i] + " <=> " + DBmap[7][i];

    qDebug() << "map length is : " << DBmaplength ;
    qDebug() << "map length is : " << DBmap[0].size() ;
    msgBox.close();
}
void MainWindow::deletalldatabase()
{
    for ( int i = 0 ; i < 16 ; i++ ) DBmap[i].clear();
    DBmapbackup.clear();
    Tableconvertiseur.clear();
}

QString MainWindow::codeofworker(QString title , QString olddate , QString newdate , int *ihelpme , bool exception)
{
    QString code ;
    if ( exception == true)
    {
        code  = title + formatdatestringforcode(DBmap[2][DBmaplength]) + "01" ;
    }
    else
    {

        if ( olddate != newdate ) *ihelpme = 1 ;

        if ( *ihelpme < 10 )
            code  = title + formatdatestringforcode(DBmap[2][DBmaplength]) + "0" + QString::number( *ihelpme ) ;
        else
            code  = title + formatdatestringforcode(DBmap[2][DBmaplength]) + QString::number( *ihelpme ) ;
    }

    return code ;
}


QString MainWindow::amerA(int index )
{
    QString a ;
    if ( ui->sreachtype->currentIndex() == 0 )  a = DBmap[7][index] ;
    else if ( ui->sreachtype->currentIndex() == 1 )  a = DBmap[8][index] ;
    else if ( ui->sreachtype->currentIndex() == 2 )  a = DBmap[5][index] + "L" ;
    else if ( ui->sreachtype->currentIndex() == 3 )  a = DBmap[2][index] + "L" ;
    else if ( ui->sreachtype->currentIndex() == 4 )  a = DBmap[4][index] ;
    else a = DBmap[6][index] ;
    return a.toUpper() ;
}
QString MainWindow::amerB()
{
    QString b ;
    QString separateddate = "-" ;

    if ( ui->sreachtype->currentIndex() == 2 || ui->sreachtype->currentIndex() ==  3 )
    {

        if ( !ui->lineEditYear->text().isEmpty() && !ui->lineEditMonth->text().isEmpty() && !ui->lineEditDay->text().isEmpty() )
            b = ui->lineEditYear->text() + separateddate + tadwir(ui->lineEditMonth->text()) + separateddate + tadwir(ui->lineEditDay->text()) ;

        else if (!ui->lineEditYear->text().isEmpty() && !ui->lineEditMonth->text().isEmpty())
            b = ui->lineEditYear->text() + separateddate + tadwir(ui->lineEditMonth->text())  ;

        else if (!ui->lineEditMonth->text().isEmpty() && !ui->lineEditDay->text().isEmpty())
            b = separateddate + tadwir(ui->lineEditMonth->text()) + separateddate + tadwir(ui->lineEditDay->text())  ;

        else if ( !ui->lineEditYear->text().isEmpty() )
            b = ui->lineEditYear->text() ;

        else if ( !ui->lineEditMonth->text().isEmpty() )
            b = separateddate + tadwir(ui->lineEditMonth->text()) + separateddate ;

        else if ( !ui->lineEditDay->text().isEmpty() )
            b = separateddate + tadwir(ui->lineEditDay->text()) + "L" ;
    }
    else
    b = ui->lineEditString->text() ;

    return b.toUpper() ;
}
QString MainWindow::fullnameoftitle(QString title)
{
    QString tmp ;

         if ( title.contains("RF") ) tmp = "SARL Ramy Food" ;
    else if ( title.contains("TF") ) tmp = "SARL Taiba Food" ;
    else if ( title.contains("UD") ) tmp = "SARL UNIVERS DETERGENT" ;
    else if ( title.contains("UC") ) tmp = "SARL UNIVER COSMETIQUE" ;
    else if ( title.contains("ES") ) tmp = "EURL SOFTY" ;

    return tmp ;
}
QString MainWindow::fullnumsocial(QString title)
{
    QString tmp ;
    /*
         if ( title == "RF") tmp = "31468081 45" ;
    else if ( title == "TF") tmp = "31468082 44" ;
    else if ( title == "UD") tmp = "31458211 58" ;
    else if ( title == "UC") tmp = "31468336 47" ;
    else if ( title == "ES") tmp = "31461957 50" ;
    */

         if ( title.contains("RF") ) tmp = part[0][0] ;
    else if ( title.contains("TF") ) tmp = part[1][0] ;
    else if ( title.contains("UD") ) tmp = part[2][0] ;
    else if ( title.contains("UC") ) tmp = part[3][0] ;
    else if ( title.contains("ES") ) tmp = part[4][0] ;

    return tmp ;
}
QString MainWindow::getaddress(QString title)
{
    QString tmp ;

         if ( title.contains("RF") ) tmp = part[0][1] ;
    else if ( title.contains("TF") ) tmp = part[1][1] ;
    else if ( title.contains("UD") ) tmp = part[2][1] ;
    else if ( title.contains("UC") ) tmp = part[3][1] ;
    else if ( title.contains("ES") ) tmp = part[4][1] ;

    return tmp ;
}
QString MainWindow::chatakhdemsocite(QString title)
{
    QString tmp ;

         if ( title.contains("RF") ) tmp = "Commerce en gros des Boissons Non Alcolise " ;
    else if ( title.contains("TF") ) tmp = "Commerce en gros des Boissons Non Alcolise " ;
    else if ( title.contains("UD") ) tmp = "Commerce en gros des produits detergent"     ;
    else if ( title.contains("UC") ) tmp = "Commerce en gros des produits detergent "    ;
    else if ( title.contains("ES") ) tmp = "Commerce en gros des Boissons Non Alcolise " ;

    return tmp ;
}

bool MainWindow::fexists(QString path)
{
    QFileInfo check_file(path);
    // check if file exists and if yes: Is it really a file and no directory?
    if (check_file.exists() && check_file.isFile()) {
        return true;
    } else {
        return false;
    }
}
void MainWindow::disablecompont()
{
    ui->actionBack_Page->setDisabled(true);
    ui->actionNext_Page->setDisabled(true);
    ui->sreachtype->setDisabled(true);
    ui->TB_sreach->setDisabled(true);

    ui->label_example_date_input->setVisible(false);
    ui->lineEditDay->setVisible(false);
    ui->lineEditMonth->setVisible(false);
    ui->lineEditYear->setVisible(false);
    ui->label_1->setVisible(false);
    ui->label_2->setVisible(false);

    ui->actionCarte_Chifa_CNAS->setDisabled(true);
    ui->actionAllocation_Familial_CNAS->setDisabled(true);
    ui->actionBulletin_De_Paie->setDisabled(true);
    ui->actionAttestation->setDisabled(true);
    ui->actionCertificat->setDisabled(true);

}
void MainWindow::enablecompont()
{
    /*
    ui->actionBack_Page->setEnabled(true);
    ui->actionNext_Page->setEnabled(true);
    */
    ui->sreachtype->setEnabled(true);
    ui->TB_sreach->setEnabled(true);

}
void MainWindow::keyPressEvent ( QKeyEvent * event )
{
     if(event->key() == Qt::Key_Return) // à chager par une auter touche
     on_TB_sreach_clicked();

 /* dange
     if(event->key() == Qt::Key_N )
     on_actionNext_triggered();
     if(event->key() == Qt::Key_B )
     on_actionBack_triggered();
*/

}
void MainWindow::deltall()
{
    QDir dir(pathfordel);
    dir.setNameFilters(QStringList() << "*.*");
    dir.setFilter(QDir::Files);
    foreach(QString dirFile, dir.entryList())
    {
        dir.remove(dirFile);
    }
}
QString MainWindow::getkholsa()
{
    chifada getvalue ;
    getvalue.setModal(true);
    getvalue.exec() ;
    return getvalue.value ;
}
QString MainWindow::getmontant(QString kholsa)
{
    QString tmp = kholsa.replace(",","") ;
    kholsa = tmp.replace(".","") ;
    int imontant = kholsa.toInt() /10000 ;
    QString result = QString::number(imontant * 9) ;

    return result[0] + "," + result[1] + result[2] + result[3] + ".00" ;
}

int MainWindow::howmuchworktoend(QString date1 ) // dd/mm/yyyy
{
    QString date2 , tmp ;
    for ( int i = 3 ; i <= 9 ; i++) date2 += today[i] ; // mm/yyyy

    tmp = date1[5] + "" + date1[6] + "/" + date1[0] + date1[1] + date1[2] + date1[3] ;

    date1 = tmp ; // mm/yyyy
    int counter ;
    for ( counter = 0 ;  ; counter++ )
    {
        //qDebug() << counter <<  "  " + date1 + " <==> " + date2 ;
        if ( date1 == date2 ) break ;
        date1 = dateconfigup(date1 , 1 ) ;
    }
    tmp = counter ;
    //qDebug() << counter ;
    return counter ;
}
int MainWindow::howmuchwork(QString date1 ) // dd/mm/yyyy
{
    QString date2 , tmp ;
    for ( int i = 3 ; i <= 9 ; i++) date2 += today[i] ; // mm/yyyy

    tmp = date1[5] + "" + date1[6] + "/" + date1[0] + date1[1] + date1[2] + date1[3] ;

    date1 = tmp ; // mm/yyyy
    int counter ;
    for ( counter = 0 ; counter < 12 ; counter++ )
    {
        qDebug() << counter <<  "  " + date1 + " <==> " + date2 ;
        if ( date1 == date2 ) break ;
        date1 = dateconfigup(date1 , 1 ) ;
    }
    tmp = counter ;
    qDebug() << counter ;
    return counter ;
}
void MainWindow::reflesh()
{
    ui->actionBack_Page->setEnabled(true);
    ui->actionNext_Page->setEnabled(true);
    ui->actionAttestation->setDisabled(true);
    ui->actionCertificat->setDisabled(true);
    ui->actionCarte_Chifa_CNAS->setDisabled(true);
    ui->actionAllocation_Familial_CNAS->setDisabled(true);
    ui->actionBulletin_De_Paie->setDisabled(true);

    if ( DBmapbackup[0].isEmpty() )
    {
        ui->actionNext_Page->setDisabled(true);
        ui->actionBack_Page->setDisabled(true);
        ui->textBrowser->clear();
        QString msg = "Resulta total est : 0 de 0 " ;
        ui->statusBar->showMessage(msg , 0 );

    }
    else
    {
        if ( currentindex == DBmapbackup.size()-1 ) ui->actionNext_Page->setDisabled(true);
        if ( currentindex == 0                    ) ui->actionBack_Page->setDisabled(true);


        ui->textBrowser->clear();
        ui->textBrowser->append(DBmapbackup[currentindex].toStdString().data()) ;
        QString msg = "Resulta total est : " + QString::number(currentindex+1) + " de " + QString::number(DBmapbackup.size())   ;
        ui->statusBar->showMessage(msg , 0 );
        int index = Tableconvertiseur[currentindex] ;
        if ( DBmap[1][index].isEmpty() )
        {
            ui->actionAttestation->setEnabled(true);
        }
        else
        {
            ui->actionCertificat->setEnabled(true);
        }
        if ( howmuchwork(DBmap[2][index]) != 0 )
        {
        ui->actionCarte_Chifa_CNAS->setEnabled(true);
        ui->actionAllocation_Familial_CNAS->setEnabled(true);
        ui->actionBulletin_De_Paie->setEnabled(true);
        }
    }

}
