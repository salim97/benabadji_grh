#include "chifada.h"
#include "ui_chifada.h"
#include <QDebug>
chifada::chifada(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::chifada)
{
    int min = 18000 ;
    int max = 30000 ;
    int step = 1000 ;
    ui->setupUi(this);
    ui->spinBox->setMinimum(min);
    ui->spinBox->setMaximum(max);
    ui->spinBox->setSingleStep(step);
    ui->spinBox->setValue(18000);
}

chifada::~chifada()
{
    delete ui;
}

void chifada::on_pushButton_clicked()
{

    value = ui->spinBox->text() ;
    close();
}


