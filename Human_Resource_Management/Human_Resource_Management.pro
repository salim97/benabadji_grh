#-------------------------------------------------
#
# Project created by QtCreator 2016-07-04T16:51:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include(xlsx/qtxlsx.pri)

TARGET = Human_Resource_Management
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    options.cpp \
    mes_propres_methodes.cpp\
    chifada.cpp \
    mes_propres_methodes_de_control.cpp

HEADERS  += mainwindow.h \
    options.h\
    chifada.h

FORMS    += mainwindow.ui \
    options.ui\
    chifada.ui

RESOURCES += \
    resource.qrc
CONFIG += c++11
