#include "options.h"
#include "ui_options.h"
#include <QString>
#include <QDebug>

Options::Options(QWidget *parent ) :
    QDialog(parent),
    ui(new Ui::Options)
{
    ui->setupUi(this);
    readsettingfromfile(QDir::currentPath()+"/"+rec);
}

Options::~Options()
{
    delete ui;
}


void Options::on_TB_Cancel_clicked()
{
    close();
}

void Options::on_TB_OK_clicked()
{
    savesettingtofile();
    close();
}

void Options::on_TB_Apply_clicked()
{
    savesettingtofile();
}
void Options::savesettingtofile()
{
    QFile::remove(QDir::currentPath()+"/"+rec);
    QFile file( QDir::currentPath()+"/"+rec );
    if ( !file.open(QIODevice::WriteOnly | QIODevice::Text) )
    {

    qDebug() << "can not open file to write" ;
    QMessageBox::warning(this,"erreur","erreur while opening file write only");
    }

    QTextStream stream( &file );
    stream << ui->lineEdit_1->text() + "=" + ui->lineEdit_1_6->text() + "\n";
    stream << ui->lineEdit_2->text() + "=" + ui->lineEdit_2_7->text() + "\n";
    stream << ui->lineEdit_3->text() + "=" + ui->lineEdit_3_8->text() + "\n";
    stream << ui->lineEdit_4->text() + "=" + ui->lineEdit_4_9->text() + "\n";
    stream << ui->lineEdit_5->text() + "=" + ui->lineEdit_5_10->text() + "\n";
    file.close();
}
void Options::readsettingfromfile(QString path)
{

    QFile mfile(path) ;
    if(!mfile.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "could not open file for read mode" ;
        QMessageBox::warning(this,"erreur","could not open file for read mode");
        return ;
    }
    QTextStream in(&mfile);
    for (int i = 1 ; !in.atEnd() ; i++ )
    {
        QString line = in.readLine();
        QStringList parts = line.split("=", QString::KeepEmptyParts) ;
        if ( i == 1 )
        {
            ui->lineEdit_1->setText(parts[0]);
            ui->lineEdit_1_6->setText(parts[1]);
        }
        if ( i == 2 )
        {
            ui->lineEdit_2->setText(parts[0]);
            ui->lineEdit_2_7->setText(parts[1]);
        }
        if ( i == 3 )
        {
            ui->lineEdit_3->setText(parts[0]);
            ui->lineEdit_3_8->setText(parts[1]);
        }
        if ( i == 4 )
        {
            ui->lineEdit_4->setText(parts[0]);
            ui->lineEdit_4_9->setText(parts[1]);
        }
        if ( i == 5 )
        {
            ui->lineEdit_5->setText(parts[0]);
            ui->lineEdit_5_10->setText(parts[1]);
        }


    }

}

void Options::on_restart_clicked()
{
   readsettingfromfile(":/setting/Setting File/setting.ini") ;
}
